# gEngine
A graphic renderer in ASCII, written in C++.

![](https://i.imgur.com/3vbuWXR.mp4)

## Get Started
1. `cmake -R .`
1. `make`
1. Enjoy!

## The code
The coding may be improved in numerous ways. If you want to help, feel free to submit a pull request.
