#pragma once

#include "utils/vector.h"

// TODO control of the perpendicularity of directions (2 saved, 1 implied)
struct Entity {
	Entity();
	Entity(const Vec3&, const Vec3&, const Vec3&);
	Entity(const Entity&);
	void rotate(const Vec3&, const Ang3&);
	void setFront(const Vec3&);
	void setUp(const Vec3&);
	void setPosition(const Vec3&);
	void translate(const Vec3&);
	virtual bool contains(const Vec3&) const = 0;

	const Vec3 position() const;
	const Vec3 up();
	const Vec3 front();
	const Vec3 right();
	
	void debug(std::string name = "Entity");

	protected:
		void resetUpDirFromFrontDir();

		Vec3 pos;
		Vec3 frontDir;
		Vec3 upDir;
};

struct UntouchableEntity : public Entity {
	UntouchableEntity() : Entity() {};
	UntouchableEntity(const Vec3& nPos, const Vec3& nFD, const Vec3& nUD) : Entity(nPos, nFD, nUD) {};
	virtual bool contains(const Vec3&) const { return false; };
};
