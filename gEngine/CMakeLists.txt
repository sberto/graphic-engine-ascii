# This is a minimal CMakeLists file which includes the Yoctopuce C++ lib.
cmake_minimum_required (VERSION 3.16)
add_subdirectory (utils)


# Create a library called "Hello" which includes the source file "hello.cxx".
# The extension is already found. Any number of sources could be listed here.
add_library (gEngine_lib entity.cpp gEngine.cpp object.cpp player.cpp screen.cpp)

target_link_libraries (gEngine_lib LINK_PUBLIC utils_lib)

# Make sure the compiler can find include files for our Hello library
# when other libraries or executables link to Hello
target_include_directories (gEngine_lib PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
