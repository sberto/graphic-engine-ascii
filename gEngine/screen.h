#pragma once

#include "player.h"
#include <string>
#include <vector>

namespace Object
{
	class Object;
}

struct Scene;

namespace Screen {

	enum Color { White, Green, Red, Blue, Black, C1, C2 };
	enum State { Hit, Missed };

	struct HitPrintable
	{
		HitPrintable(const Color& color, const State& state) : myColor{color}, myState{state} {};
		Color myColor;
		State myState;
	};

	struct Pixel : public UntouchableEntity {
		void set(HitPrintable);
		void setBlank();

		std::string print();

		Pixel(const Vec3& newPos, const Vec3& newDir);
		Pixel();

		private:
			Color color;
			State state;
	};

	struct Screen 
	{

		Screen(Player*, unsigned int, unsigned int);

		Pixel* at(int, int);
		std::string print();	

		unsigned int width();
		unsigned int height();

		private:
			Player* player;
			std::vector<std::vector<Pixel>> pixels;
			const unsigned int myWidth;
			const unsigned int myHeight;
			const Vec3::metric myDistanceFromPlayer;
	};

	struct Ray : public UntouchableEntity {
		Ray();
		Ray(Pixel*);

		HitPrintable getHitPrintable(Scene*);
		void propagate();
		inline int propagationMaxTimes() { return (int) (MaxDistance / STEP); }

		private:
			double MaxDistance;
			static constexpr double STEP = 1;
	};
}
