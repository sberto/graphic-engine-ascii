#include "screen.h"
#include "printable.h"
#include <iostream>
#include "scene.h"
#include "object.h"

Screen::Screen::Screen(Player* p, unsigned int w, unsigned int h) :
	player{p},
	pixels{w, std::vector<Pixel>(h)},
	myWidth{w},
	myHeight{h},
	myDistanceFromPlayer{5}
{
	if (p == nullptr) 
	{
		std::cout << "Player is null.\n";
		return;
	}

	for (unsigned int j=0; j<myHeight; j++) 
		for (unsigned int i=0; i<myWidth; i++) 
		{
			// temporary. TODO
			const Vec3 screenPos = p->position() + p->front() * myDistanceFromPlayer;
			const double mult = 0.3;
			const double multV = 0.4;
			const double multH = 0.5;
			const Vec3 pixelPos = screenPos + p->up() * mult * multV * ((double)j - (double)myHeight/2.0) + p->right() * mult * multH * ((double)i - (double)myWidth/2.0); 

			pixels[i][j].setPosition(pixelPos);
			pixels[i][j].setFront(pixels[i][j].position() - p->position());

			pixels[i][j].debug("Pixel" + std::to_string(i) + ":"  + std::to_string(j));
		}
}

Screen::Pixel::Pixel(): UntouchableEntity() { }

Screen::Pixel::Pixel(const Vec3& newPos, const Vec3& newDir) :
	color{Color::White},
	state{State::Missed},
	UntouchableEntity{newPos, newDir, Vec3{0,0,1}}
{
	debug("Pixel");
}

void Screen::Pixel::setBlank()
{
	color = Color::White;
	state = State::Missed;
}

void Screen::Pixel::set(HitPrintable data)
{
	color = data.myColor;
	state = data.myState;
}

std::string Screen::Pixel::print()
{
	if (state == State::Missed)
		return Print::Symbol::Missed;

	std::string hitString;
	switch (color)
	{
		case White: hitString += Print::Color::White; break;
		case Green: hitString += Print::Color::Green; break;
		case Red:   hitString += Print::Color::Red; break;
		case Blue:  hitString += Print::Color::Blue; break;
		case Black: hitString += Print::Color::Black; break;
		case C1: hitString += Print::Color::C1; break;
		case C2: hitString += Print::Color::C2; break;
		default:    hitString += Print::Color::Black; break;
	}

	hitString += Print::Symbol::Hit;
	return hitString;
}

std::string Screen::Screen::print()
{
	std::string toReturn;

	toReturn += " ";
	for (unsigned int j=0; j<myHeight; j++) 
		toReturn += Print::Color::Regular+"__";
	toReturn += "\n";

	for (unsigned int j=0; j<myHeight; j++) 
	{
		toReturn += Print::Color::Regular+"|";
		for (unsigned int i=0; i<myWidth; i++) 
		{
			toReturn += pixels[i][j].print();
			toReturn += " ";
		}
		toReturn += Print::Color::Regular+"|";
		toReturn += "\n";
	}

	toReturn += " ";
	for (unsigned int j=0; j<myHeight; j++) 
		toReturn += Print::Color::Regular+"__";
	toReturn += "\n";

	toReturn += Print::Color::Regular;

	return toReturn;
}	


unsigned int Screen::Screen::width()
{
	return myWidth;
}

unsigned int Screen::Screen::height()
{
	return myHeight;
}

Screen::Pixel* Screen::Screen::at(int x, int y)
{
	return &pixels[x][y];
}

Screen::Ray::Ray(Pixel* pixel) :
	UntouchableEntity(pixel->position(), pixel->front(), pixel->up()),
	MaxDistance{20}
{
}

Screen::HitPrintable Screen::Ray::getHitPrintable(Scene* scn)
{
	for (int i=0; i<propagationMaxTimes(); i++) {
		propagate();
		for (const Object::Object* thisObj : scn->list())
		{
			if (thisObj->contains(pos))
			{
				return HitPrintable{ thisObj->hitColor(pos), State::Hit };
			}
		}
	}
	return HitPrintable{ Color::White, State::Missed };
}

void Screen::Ray::propagate()
{
	pos += frontDir * STEP;
}
