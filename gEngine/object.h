#pragma once

#include "screen.h"
#include "entity.h"
#include <vector>

namespace Ray
{
	struct Ray;
}

namespace Object
{
	struct Object : public Entity {
		Object();
		Object(const Vec3&, const Vec3&, const Vec3&);

		virtual bool isVisible() const { return amIVisible; };

		virtual bool contains(const Vec3&) const = 0;
		virtual Screen::Color hitColor(const Vec3&) const = 0;

		protected:
			bool amIVisible;
	};

	struct Box : public Object {
		Box(Vec3::metric);
		Box(const Vec3&, const Vec3&, const Vec3&, const std::vector<Vec3::metric>& dim_v);

		virtual bool contains(const Vec3&) const;
		virtual Screen::Color hitColor(const Vec3&) const;

		private:
			Vec3::metric length;
	};

	struct Sphere : public Object {
		Sphere(Vec3::metric);
		Sphere(const Vec3&, const Vec3&, const Vec3&, const std::vector<Vec3::metric>& dim_v);

		virtual bool contains(const Vec3&) const;
		virtual Screen::Color hitColor(const Vec3&) const;

		private:
			Vec3::metric radius;
	};
}
