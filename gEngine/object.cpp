#include "object.h"
#include <iostream>
#include <string>

Object::Object::Object():
	Entity(),
	amIVisible(true)
{
}

Object::Object::Object(const Vec3& nP, const Vec3& nD1, const Vec3& nD2):
	Entity(nP, nD1, nD2),
	amIVisible(true)
{
}

bool Object::Box::contains(const Vec3& point) const
{
	const Vec3 pointRefBox = point - pos; 
	const Vec3 rightDir = frontDir.cross(upDir).normal();

	const double projFront = pointRefBox.dot(frontDir);
	const double projUp = pointRefBox.dot(upDir);
	const double projRight = pointRefBox.dot(rightDir);


	return (-length < projFront && projFront < length &&
		-length < projUp    && projUp    < length &&
		-length < projRight && projRight < length);
}

int min2 (const double x, const double y) { if (x<y) return 0; return 1; }
int min (const double x, const double y, const double z) { 
	if (min2(x,y) == 0)
	{
	       if (min2(x,z) == 0) return 0; 
	       else return 2;
	}
	else
	{
	       if (min2(y,z) == 0) return 1; 
	       else return 2;
	}
}


Screen::Color Object::Box::hitColor(const Vec3& point) const
{
	const Vec3 pointRefBox = point - pos; 
	const Vec3 rightDir = frontDir.cross(upDir).normal();

	double F = (pointRefBox.dot(frontDir));
	double U = (pointRefBox.dot(upDir));
	double R = (pointRefBox.dot(rightDir));
	F = (F > 0) ? (F - length) : (F + length);
	U = (U > 0) ? (U - length) : (U + length);
	R = (R > 0) ? (R - length) : (R + length);
	F = abs(F);
	U = abs(U);
	R = abs(R);

	switch (min(F, U, R))
	{
		case 0: return Screen::Color::Red; break;
		case 1: return Screen::Color::Blue; break;
		case 2: return Screen::Color::Green; break;
	}
}

Object::Box::Box(Vec3::metric newLen) :
	Object(),
	length{newLen}
{
}

Object::Box::Box(const Vec3& newPos, const Vec3& newFront, const Vec3& newUp, const std::vector<Vec3::metric>& dim_v):
	Object(newPos, newFront, newUp),
	length{dim_v.size() == 1? dim_v[0] : 1}
{
	debug("Box");
}

bool Object::Sphere::contains(const Vec3& point) const
{
	const Vec3 pointRefSphere = point - pos; 

	return pointRefSphere.norm() < radius;
}

Screen::Color Object::Sphere::hitColor(const Vec3& point) const
{
	const Vec3 pointRefSphere = point - pos; 
	const Vec3 rightDir = frontDir.cross(upDir).normal();

	double F = (pointRefSphere.dot(frontDir));
	double U = (pointRefSphere.dot(upDir));
	double R = (pointRefSphere.dot(rightDir));
	F = (F > 0) ? (F - radius) : (F + radius);
	U = (U > 0) ? (U - radius) : (U + radius);
	R = (R > 0) ? (R - radius) : (R + radius);
	F = abs(F);
	U = abs(U);
	R = abs(R);

	switch (min(F, U, R))
	{
		case 0: return Screen::Color::C1; break;
		case 1: return Screen::Color::C2; break;
		case 2: return Screen::Color::White; break;
	}
}

Object::Sphere::Sphere(Vec3::metric newRadius) :
	Object(),
	radius{newRadius}
{
}

Object::Sphere::Sphere(const Vec3& newPos, const Vec3& newFront, const Vec3& newUp, const std::vector<Vec3::metric>& dim_v):
	Object(newPos, newFront, newUp),
	radius{dim_v.size() == 1? dim_v[0] : 1}
{
	debug("Sphere");
}
