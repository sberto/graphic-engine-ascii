#pragma once

#include <string>
#include "screen.h"
#include "scene.h"
#include "object.h"
#include "player.h"


struct gEngine{
		static gEngine* instance();

		std::string getFrame();

		Scene* scene() { return myScene; };

	private:
		gEngine();

		Screen::Screen* myScreen;

		Player* myPlayer;

		Scene* myScene;
};
