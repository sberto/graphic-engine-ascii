#include "vector.h"
#include <math.h>

Vec3::Vec3() :
	x{0},
	y{0},
	z{0}
{}

Vec3::Vec3(metric newX, metric newY, metric newZ) :
	x{newX},
	y{newY},
	z{newZ}
{
}

Vec3::Vec3(const Vec3& v)
{
	x = v.x;
	y = v.y;
	z = v.z;
}

const Vec3 Vec3::cross(const Vec3& v) const
{
	return Vec3{y*v.z-z*v.y, z*v.x - x*v.z, x*v.y - y*v.x};		
}

const double Vec3::dot(const Vec3& v) const
{
	return x*v.x + y*v.y + z*v.z;
}

std::ostream& operator <<(std::ostream& str, const Vec3& v)
{
	return	str << ("[" + std::to_string(v.x) + ", "
		+ std::to_string(v.y) + ", "
		+ std::to_string(v.z) + "]");
}

const Vec3::metric Vec3::norm() const
{
	return std::sqrt(x*x + y*y + z*z);
}

const Vec3 Vec3::normal() const
{
	return (*this / norm());
}

const Vec3 Vec3::operator/ (const metric times) const
{
	return Vec3{x/times, y/times, z/times};
}

const Vec3 Vec3::operator* (const metric times) const
{
	return Vec3{x*times, y*times, z*times};
}

const Vec3 Vec3::operator+ (const Vec3& other) const
{
	return Vec3{x+other.x, y+other.y, z+other.z};
}

const Vec3 Vec3::operator- (const Vec3& other) const
{
	return Vec3{x-other.x, y-other.y, z-other.z};
}

bool Vec3::operator== (const Vec3& v) const
{
	return (x == v.x && y == v.y && z == v.z);
}

bool Vec3::operator!= (const Vec3& v) const
{
	return !(x == v.x && y == v.y && z == v.z);
}

const Vec3& Vec3::operator+= (const Vec3& other)
{
	return *this = *this + other;
}
