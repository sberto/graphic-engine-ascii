#pragma once
#include <string>
struct Ang3;

struct Vec3 {
	typedef double metric;

	Vec3();
	Vec3(metric, metric, metric);
	
	Vec3(const Vec3&);
	//! \brief Rotate
	//!
	//! \param Rotation point and angle in radians
	Vec3& rotate(const Vec3&, double);
	Vec3& rotate(const Ang3&);

	Vec3& translate(const Vec3&);

	const Vec3 normal() const;
	const metric norm() const;

	const Vec3 operator/ (const metric) const;
	const Vec3 operator* (const metric) const;
	const Vec3 operator+ (const Vec3&) const;
	const Vec3 operator- (const Vec3&) const;
	const Vec3& operator+= (const Vec3&);
	bool operator== (const Vec3&) const;
	bool operator!= (const Vec3&) const;

	friend std::ostream& operator <<(std::ostream&, const Vec3&);

	const Vec3 cross(const Vec3&) const;
	const double dot(const Vec3&) const;

	protected:
		metric x;
		metric y;
		metric z;
};

struct Ang3 : Vec3 {
};
