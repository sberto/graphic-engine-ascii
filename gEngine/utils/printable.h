namespace Print
{
	namespace Symbol
	{
		const std::string Missed = " ";
		const std::string Hit = "O";
	}

	namespace Color
	{
		/*
		 *      foreground background
		 *	black        30         40
		 *	red          31         41
		 *	green        32         42
		 *	yellow       33         43
		 *	blue         34         44
		 *	magenta      35         45
		 *	cyan         36         46
		 *	white        37         47
		 */
		const std::string Regular = "\033[0;37m";
		const std::string White = "\033[1;37m";
		const std::string Green = "\033[1;32m";
		const std::string Red   = "\033[1;31m";
		const std::string Blue  = "\033[1;34m";
		const std::string C1  = "\033[1;35m";
		const std::string C2  = "\033[1;36m";
		const std::string Black = "\033[1;30m";
	}
}
