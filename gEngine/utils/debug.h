#include <iostream>

struct DebugStream {};

extern DebugStream debugLog;

template <typename T> DebugStream& operator<< (DebugStream &s, const T &x) {
	std::cout << x;
	return s;
}
