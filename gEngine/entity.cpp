#include "entity.h"
#include <iostream>

void Entity::debug(std::string name)
{
	std::cout << name << "\t" << "Pos: " << position() << "\n";
	std::cout << name << "\t" << "Front: " << front() << "\n";
	std::cout << name << "\t" << "Up: " << up()       << "\n";
	std::cout << name << "\t" << "Right: " << right() << "\n";
	std::cout  << "\n";
}

Entity::Entity() :
	pos{Vec3(0,0,0)},
	frontDir{Vec3(1,0,0)},
	upDir{Vec3(0,0,1)}
{
}

Entity::Entity(const Vec3& newPos, const Vec3& newFront, const Vec3& newUp):
	pos{newPos},
	frontDir{newFront.normal()},
	upDir{newUp.normal()}
{
	resetUpDirFromFrontDir();
}

Entity::Entity(const Entity& other):
	pos{other.pos},
	frontDir{other.frontDir},
	upDir{other.upDir}
{}

void Entity::resetUpDirFromFrontDir()
{
	Vec3 rightDir = frontDir.cross(upDir);
	if (rightDir == Vec3{0,0,0})
	{
		if (frontDir.cross(Vec3{1,0,0}) != Vec3{0,0,0})
			rightDir = Vec3{1,0,0};
		else rightDir = Vec3{0,1,0};
	}
	rightDir = rightDir.normal();
	upDir = rightDir.cross(frontDir).normal();
}

void Entity::setUp(const Vec3& newDir)
{
	upDir = newDir.normal();	
}

void Entity::setFront(const Vec3& newDir)
{
	frontDir = newDir.normal();	
	resetUpDirFromFrontDir();
}

void Entity::setPosition(const Vec3& newPos)
{
	pos = newPos;
}

void Entity::translate(const Vec3& input)
{
	pos += input;	
}

const Vec3 Entity::position() const
{
	return pos;
}

const Vec3 Entity::up()
{
	return upDir;
}

const Vec3 Entity::front()
{
	return frontDir;
}

const Vec3 Entity::right()
{
	return frontDir.cross(upDir).normal();
}

