#pragma once

#include <vector>
#include "vector.h"

namespace Object
{
	class Object;
}

struct Scene {
	Scene() {};

	template <typename T> T* create(Vec3 const & v1, Vec3 const & v2, Vec3 const & v3, const std::vector<Vec3::metric>& dim_v) {
		T* newElem = new T(v1, v2, v3, dim_v);
		data.push_back(newElem);
		return newElem;
	}

	std::vector<Object::Object*>& list() { return data; };

	private:
		std::vector<Object::Object*> data;
};
