#include "gEngine.h"

gEngine::gEngine() :
	myPlayer(new Player),
	myScreen(nullptr),
	myScene(new Scene)
{
	myScreen = (new Screen::Screen(myPlayer, 70,70));
}

gEngine* gEngine::instance() 
{
	return new gEngine();
}

std::string gEngine::getFrame() {
	for (unsigned int i=0; i<myScreen->width(); i++) 
	{
		for (unsigned int j=0; j<myScreen->height(); j++)
		{
			Screen::Ray newRay(myScreen->at(i,j));
			myScreen->at(i,j)->set(newRay.getHitPrintable(myScene));
		}
	}

	return myScreen->print();
}
