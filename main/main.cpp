#include <iostream>
#include "gEngine.h"
#include <cmath>

int main() {
	gEngine* engine = gEngine::instance();
	
	Object::Object* myBox = engine->scene()->create<Object::Box>(Vec3{10,0,0}, Vec3{0,1,0}, Vec3{0,0,1}, {4});
	Object::Object* mySphere = engine->scene()->create<Object::Sphere>(Vec3{14,-10,0}, Vec3{0,1,0}, Vec3{0,0,1}, {4});
	const int max = 50;
	for (int i=0; i<max*5; i++)
	{
		std::cout << engine->getFrame();
		mySphere->translate(Vec3{-cos(((double)i)/max*3.14) * 0.3,sin(((double)i)/max*3.14) * 0.8,0});
		myBox->translate(Vec3{sin(((double)i)/max*3.14) * 0.3,0,0});
		myBox->translate(Vec3{0, 0, cos(((double)i*2)/max*3.14) * 0.3});
		myBox->setFront(Vec3{sin((double)(i)/max*3.14) * 4* cos((double)(i)/max*3.14) , sin((double)(i)/max*3.14), cos((double)(i)/max*3.14)});
		myBox->debug();
	}
	//engine->nextFrame();
}
